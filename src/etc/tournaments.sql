drop table tournaments
create table tournaments
(
  tournamentid int primary key NOT NULL,
  name varchar(30),
  starttime Date,
  endtime Date,
  leaguename varchar(30),
  status bit
)