package is.rufan.tournament.service;

/**
 * @author Hrafnkell Baldursson
 * @author Rannveig Gu�mundsd�ttir
 * This class represents an exception in the GameService.
 */
public class TournamentServiceException extends RuntimeException
{
    public TournamentServiceException() { }

    public TournamentServiceException(String message)
    {
        super(message);
    }

    public TournamentServiceException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
