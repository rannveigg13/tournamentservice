package is.rufan.tournament.service;

import com.sun.org.apache.xml.internal.serializer.ToUnknownStream;
import is.rufan.tournament.domain.Tournament;

import java.util.List;

/**
 * @author Hrafnkell Baldursson
 * @author Rannveig Gu�mundsdottir
 * This interface represents the service layer for Tournaments.
 */
public interface TournamentService {
    /**
     * Gets a list of all Tournaments.
     * @return a list of all tournaments in the database.
     */
    List<Tournament> getTournaments();

    /**
     * Gets a list of all active Tournaments.
     * @return a list of active Tournaments in the database.
     */
    List<Tournament> getActiveTournaments();

    Tournament getTournamentById(int tId);

}
