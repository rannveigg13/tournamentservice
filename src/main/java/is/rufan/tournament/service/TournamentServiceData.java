package is.rufan.tournament.service;

import is.rufan.tournament.data.TournamentData;
import is.rufan.tournament.data.TournamentDataGateway;
import is.rufan.tournament.domain.Tournament;
import is.ruframework.data.RuDataAccessFactory;
import is.ruframework.domain.RuException;

import java.util.List;
/**
 * @author Hrafnkell Baldursson
 * @author Rannveig Gu�mundsd�ttir
 * This class represents the service layer that uses the
 * TournamentDataGateway to get Tournament data from the database.
 */
public class TournamentServiceData implements TournamentService{

    RuDataAccessFactory factory;
    TournamentDataGateway tournamentDataGateway;

    /**
     * Sets the member variables needed for tha class.
     * @throws RuException
     */
    public TournamentServiceData() throws RuException  {
        factory = RuDataAccessFactory.getInstance("tournamentdata.xml");
        tournamentDataGateway = (TournamentDataGateway) factory.getDataAccess("tournamentData");
    }

    /**
     * Getsa list of all Tournaments.
     * @return a list of all Tournamanets in the database
     */
    public List<Tournament> getTournaments() {
        return tournamentDataGateway.getTournaments();
    }

    /**
     * Gets all the active Tournaments.
     * @return a list of all active Tournaments in the database.
     */
    public List<Tournament> getActiveTournaments() {
        return tournamentDataGateway.getActiveTournaments();
    }

    public Tournament getTournamentById(int tId){
        return tournamentDataGateway.getTournamentById(tId);
    }
}
