package is.rufan.tournament.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Hrafnkell Baldursson
 * @author Rannveig Gu�mundsd�ttir
 * This class represents an exception in the GameService.
 */
public class Tournament{

    protected int tournamentid;
    protected String name;
    protected Date starttime;
    protected Date endtime;
    protected String leaguename;
    protected Boolean status;


    public int getTournamentid() { return tournamentid; }

    public void setTournamentid(int tournamentid) { this.tournamentid = tournamentid; }

    public Date getEndTime() { return endtime; }

    public void setEndTime(Date endtime) { this.endtime = endtime; }

    public String getLeagueName() { return this.leaguename; }

    public void setLeagueName(String leaguename) { this.leaguename = leaguename; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Date getStartTime() {
        return starttime;
    }

    public String getStartTimeFormatted(){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        return format.format(starttime);
    }

    public String getEndTimeFormatted(){
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        return format.format(endtime);
    }

    public void setStartTime(Date starttime) { this.starttime = starttime; }

    public Boolean getStatus() { return status; }

    public void setStatus(Boolean status) { this.status = status; }
}
