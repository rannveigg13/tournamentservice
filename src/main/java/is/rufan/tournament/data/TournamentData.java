package is.rufan.tournament.data;

import is.rufan.tournament.domain.Tournament;
import is.ruframework.data.RuData;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Hrafnkell Baldursson
 * @author Rannveig Gu�mundsdottir
 * This class represents the layer that talks to the tournaments database table
 */
public class TournamentData extends RuData implements  TournamentDataGateway {

    /**
     * Maps values from the all tournaments rows in the tournaments database table.
     * @return A list of all Tournaments registered in the database, active or not.
     */
    public List<Tournament> getTournaments() {
        String sql = "select * from tournaments";
        JdbcTemplate queryPosition = new JdbcTemplate(getDataSource());
        List<Tournament> tournaments = queryPosition.query(sql,
                new TournamentRowMapper());
        return tournaments;
    }

    public Tournament getTournamentById(int tId){

        String sql = "select * from tournaments where tournamentid = ?";
        JdbcTemplate queryPosition = new JdbcTemplate(getDataSource());
        try{
            Tournament tournament = queryPosition.queryForObject(sql, new Object[]{tId},
                    new TournamentRowMapper());
            return tournament;
        }
        catch(EmptyResultDataAccessException ex){
            return null;
        }
    }

    /**
     * Maps values from the all active tournaments rows in the tournaments database table.
     * Updates all tournaments to closed if the current date has surpassed the start date.
     * @return A list of all currently active Tournaments registered in the database.
     */
    public List<Tournament> getActiveTournaments() {

        Date now = new Date();
        String sql = "select * from tournaments where status = 1";
        JdbcTemplate queryPosition = new JdbcTemplate(getDataSource());
        List<Tournament> activeTournaments = queryPosition.query(sql,
                new TournamentRowMapper());
        List<Tournament> tournamentsToClose = new ArrayList<Tournament>();
        for(Tournament t : activeTournaments){
            if(now.after(t.getStartTime())){
                activeTournaments.remove(activeTournaments.indexOf(t));
                tournamentsToClose.add(t);
            }
        }

        if(tournamentsToClose.size() > 0){
            closeTournaments(tournamentsToClose);
        }

        return activeTournaments;
    }

    /**
     * A helper function that sets the status of all tournaments in the given closeTournaments
     * list to closed.
     * @param closeTournaments A list containing all tournaments to be set to closed.
     */
    private void closeTournaments(List<Tournament> closeTournaments){
        JdbcTemplate queryTournament = new JdbcTemplate(getDataSource());
        int[] types = {Types.BIT, Types.INTEGER};

        for(Tournament t : closeTournaments){
            Object [] params = {t.getStatus(), t.getTournamentid()};
            queryTournament.update("UPDATE tournaments SET status = ? where tournamentid = ?",
                    params, types);
        }
    }
}
