package is.rufan.tournament.data;

import is.rufan.tournament.domain.Tournament;
import is.ruframework.data.RuDataAccess;

import java.util.List;

/**
 * @author Hrafnkell Baldursson
 * @author Rannveig Gu�mundsdottir
 * This interface represents the layer that talks to the tournaments database table
 */
public interface TournamentDataGateway extends RuDataAccess {

    /** Gets all tournaments, active or not.
     * @return A list of all Tournaments registered in the database, active or not.
     */
    List<Tournament> getTournaments();

    /** Gets all currently active tournaments. Updates the tournament rows in the
     * database that are supposed to be closed.
     * @return A list of all currently active Tournaments registered in the database.
     */
    List<Tournament> getActiveTournaments();

    Tournament getTournamentById(int tId);
}
