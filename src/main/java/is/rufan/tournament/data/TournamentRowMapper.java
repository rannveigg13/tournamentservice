package is.rufan.tournament.data;

import is.rufan.tournament.domain.Tournament;
import is.ruframework.data.RuDataAccessFactory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Hrafnkell Baldursson
 * @author Rannveig Gu�mundsdottir
 * This class represents a RowMapper that retrieves data
 * from the tournaments database table.
 */
public class TournamentRowMapper implements RowMapper<Tournament> {

    /**
     * Translates the given row from the tournaments database table into
     * a Tournament object.
     * @param rs The given row from the tournaments database table.
     * @param rowNum The number of the row.
     * @return A Tournament object constructed from the database row.
     * @throws SQLException
     */
    public Tournament mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        Tournament tournament = new Tournament();

        tournament.setTournamentid(rs.getInt("tournamentid"));
        tournament.setName(rs.getString("name"));
        tournament.setStartTime(rs.getDate("starttime"));
        tournament.setLeagueName(rs.getString("leaguename"));
        tournament.setEndTime(rs.getDate("endtime"));
        tournament.setStatus(rs.getBoolean("status"));

        return tournament;
    }
}